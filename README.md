A tool to help update and bundle galaxy toolshed repositories. 

The general idea is to automate the process of updating repository definitions such as this

```xml
<repository toolshed="http://toolshed.g2.bx.psu.edu" name="proteomics_datatypes" owner="iracooke" changeset_revision="463328a6967f"/>
```

by (a) updating changeset revisions to the latest revision, or (b) switching to an alternate toolshed url

Note that for this to work you will need to give your repositories identical names on each toolshed

## Usage Examples

- Automatically commit latest changes to a specified repository

		bundle_repo.rb ./path/to/repository  -t 'http://localrepo:9009'  -m mercurial -d 'http://username@localrepo:9009/repos/username/reponame' -a -M "Latest changes"

- Bundle a repository that is already ready for upload and check validity of all its dependency definitions
	
		bundle_repo.rb ./path/to/repository

- Update changeset revisions for any dependencies to the latest revision

		bundle_repo.rb -u ./path/to/repository

- Switch dependency definitions to the latest revision on an alternate toolshed

		bundle_repo.rb -t \'http://localtoolshedurl\' ./path/to/repository
