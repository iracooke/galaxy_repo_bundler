#!/usr/bin/env ruby
#
# Bundle a toolshed repository for uploading, 
# including translating repository dependencies between repos
#
# Assumes you have bzip2 and mercurial installed
#
require 'ostruct'
require 'optparse'
require 'pathname'
require 'tmpdir'
require 'open4'
require 'libxml'
require 'URI'

include LibXML

def check_repo_status repo_dir
	unless repo_dir!=nil 
		$stdout.write "You must supply a repository\n#{$option_parser.help}"
	end

	hg_status = %x[cd #{repo_dir};hg status]
	throw "#{repo_dir} is not a mercurial repository" if hg_status=~/hg not found/
end

def check_dependency_exists dependency
	toolshed_url = dependency.attributes['toolshed']
	owner = dependency.attributes['owner']
	name = dependency.attributes['name']
	revision = dependency.attributes['changeset_revision']
	dep_uri = URI("#{toolshed_url}/repos/#{owner}/#{name}")
	dep_uri.user=owner
	cmd = "hg identify -r #{revision} #{dep_uri.to_s}"
	puts cmd
	result = %x[#{cmd}]
	if $?.exitstatus!=0
		puts "An error occurred checking for dependency #{dependency.to_s}"
		puts "This is most likely because the dependency is not defined on the specified toolshed"
		exit
	end
	throw "#{dependency.to_s} does not exist" unless result.chomp==revision
end

def check_dependencies_exist dep_doc
	repo_deps = dep_doc.find('//repositories/repository')
	package_deps = dep_doc.find('//package/repository')
	repo_deps.each { |dependency|  check_dependency_exists(dependency) }
	package_deps.each { |dependency|  check_dependency_exists(dependency) }
end

def update_dependency_revision dependency
	toolshed_url = dependency.attributes['toolshed']
	owner = dependency.attributes['owner']
	name = dependency.attributes['name']
	dep_uri = URI("#{toolshed_url}/repos/#{owner}/#{name}")
	dep_uri.user=owner
	cmd="hg identify #{dep_uri.to_s}"
	puts cmd
	revision = %x[#{cmd}]
	if $?.exitstatus!=0
		puts "An error occurred updating dependency #{dependency.to_s}"
		puts "This is most likely because the dependency is not defined on the specified toolshed"
		exit
	end
	revision.chomp!

	dependency.attributes['changeset_revision'] = revision
end

def update_dependency_revisions dep_doc
	repo_deps = dep_doc.find('//repositories/repository')
	package_deps = dep_doc.find('//package/repository')
	repo_deps.each { |dependency| update_dependency_revision dependency  }
	package_deps.each { |dependency| update_dependency_revision dependency  }
end

def update_dependency_toolsheds dep_doc, toolshed
	repo_deps = dep_doc.find('//repositories/repository')
	package_deps = dep_doc.find('//package/repository')
	repo_deps.each { |dependency| dependency.attributes['toolshed'] = toolshed  }
	package_deps.each { |dependency| dependency.attributes['toolshed'] = toolshed  }
end

def update_repository_dependencies clonedir
	dependencies_file = "#{clonedir}/repository_dependencies.xml"
	return unless File.exist? dependencies_file
	doc = parse_dependencies(dependencies_file)

	update_dependency_toolsheds(doc,$options.toolshed) if $options.toolshed != nil
	update_dependency_revisions(doc) if $options.update || ($options.toolshed !=nil)
	check_dependencies_exist(doc)
	doc.save(dependencies_file)
end

def update_tool_dependencies clonedir
	dependencies_file = "#{clonedir}/tool_dependencies.xml"
	return unless File.exist? dependencies_file
	

	doc = parse_dependencies(dependencies_file)

	update_dependency_toolsheds(doc,$options.toolshed) if $options.toolshed != nil
	update_dependency_revisions(doc) if $options.update || ($options.toolshed !=nil)
	check_dependencies_exist(doc)
	doc.save(dependencies_file)
end

def parse_dependencies path_to_repo_deps	
	return nil unless File.exist? path_to_repo_deps
	parser=XML::Parser.file("#{path_to_repo_deps}")
	doc = parser.parse
	doc
end

def clone_repo_from_destination repo_dir, tmpdir
	dest = $options.destination_repo
	cmd="hg clone #{dest} #{tmpdir}"
	puts cmd
	%x[#{cmd}]
	# Now copy changes to the clone from repo_dir
	cmd="rsync -avri --exclude='.hg*' #{repo_dir}/ #{tmpdir}"
	puts cmd
	result=%x[#{cmd}]
	puts result
end

def clone_repo repo_dir, tmpdir
	cmd="hg clone #{repo_dir} #{tmpdir}"
	puts cmd
	%x[#{cmd}]
end

def copy_repo repo_dir, tmpdir
	cmd="cp -r #{repo_dir}/* #{tmpdir}"
	puts cmd
	%x[#{cmd}]
end

def duplicate_repo repo_dir
	clonedir=nil
	if ($options.submit_mode == 'mercurial')
		if ( $options.autopush)
			clonedir=Dir.mktmpdir
		else
			dest_uri = URI($options.destination_repo)
			clonedir="./#{dest_uri.path}"
		end
		
		clone_repo_from_destination(repo_dir, clonedir)

	elsif ( $options.prefer_clone )
		tmpdir = Dir.mktmpdir
		check_repo_status(repo_dir)
		clone_repo(repo_dir, tmpdir)
		clonedir=tmpdir
	else
		tmpdir = Dir.mktmpdir
		copy_repo(repo_dir, tmpdir)
		clonedir=tmpdir
	end
	return clonedir
end

def tarzip_repo repo_dir, repo_name
	cmd = 
	"cd #{repo_dir}; 
	tar --exclude=.DS_Store --exclude=.hgcheck --exclude=.hg --exclude=*.tar -cvf #{repo_name}.tar ./;
	bzip2 #{repo_name}.tar
	"
	puts cmd
	%x[#{cmd}]
end

def options_fail message
	puts message
	puts $option_parser
	exit
end

$options = OpenStruct.new
$option_parser=OptionParser.new do |opts|
            
	$options.toolshed = nil
	opts.on( '-t', '--toolshed shed_url', 'Translate toolshed urls to a new toolshed while bundling' ) do |shed|
		$options.toolshed = shed
	end

	$options.update = false
	opts.on( '-u', '--update','Update to the latest changeset revision for each dependency. Implied when updating toolshed') do 
		$options.update = true
	end

	$options.prefer_clone = false
	opts.on( '-c', '--clone','(tarzip mode only). When this option is set the source repository will be cloned into the destination rather than copied.  Useful if you want to avoid files not yet checked in to version control') do 
		$options.prefer_clone = true
	end

	$options.submit_mode = 'tarzip'
	opts.on( '-m', '--mode value','There are two modes <tarzip> and <mercurial>. Use this to switch. Default is tarzip') do |mode|
		$options.submit_mode = mode
	end

	$options.destination_repo = nil
	opts.on( '-d', '--destination_repo repo_url','When in mercurial mode set this to the url of the mercurial repository where the updates will be pushed') do |dest|
		$options.destination_repo = dest
	end

	$options.autopush = false
	opts.on( '-a', '--auto','(mercurial mode only). Automatically commit and push changes') do 
		$options.autopush = true
	end

	$options.commitmessage = "auto"
	opts.on( '-M', '--message message','(mercurial mode only). Commit message when automatically committing and pushing changes') do |message|
		$options.commitmessage = message
	end

end

$option_parser.banner = "Bundle a toolshed repo for uploading.\n\nUsage: bundle_repo.rb [options] tool_dir"

$option_parser.parse!



repo_dir = ARGV[0]
options_fail "Path to a repository is required" unless repo_dir!=nil
options_fail "No directory at path #{repo_dir}" unless File.exist? repo_dir
unless ["tarzip","mercurial"].include? $options.submit_mode
	options_fail "Invalid mode #{$options.submit_mode}. Only tarzip and mercurial are allowed" 
end
if $options.submit_mode=="mercurial" && $options.destination_repo==nil
	options_fail "A destination repo '-d' is required when in mercurial mode"
end

repo_name = Pathname.new(repo_dir).basename.to_s

clonedir = duplicate_repo(repo_dir)

update_repository_dependencies(clonedir)

update_tool_dependencies(clonedir)

	# require 'debugger';debugger

if $options.submit_mode == 'tarzip'
	tarzip_repo(clonedir, repo_name)

	cmd="cp #{clonedir}/#{repo_name}.tar.bz2 ."
	puts cmd
	%x[#{cmd}]
	FileUtils.remove_entry_secure clonedir

elsif $options.autopush 
#	require 'debugger';debugger
	cmd="cd #{clonedir}; hg commit -m \'#{$options.commitmessage}\';hg push"
	puts cmd
 	%x[#{cmd}]
 	FileUtils.remove_entry_secure clonedir
end

